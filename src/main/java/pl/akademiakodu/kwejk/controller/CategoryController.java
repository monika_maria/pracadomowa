package pl.akademiakodu.kwejk.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.akademiakodu.kwejk.model.Category;
import pl.akademiakodu.kwejk.repository.BazaCategoryRepository;
import pl.akademiakodu.kwejk.repository.CategoryRepository;
import pl.akademiakodu.kwejk.repository.GifRepository;

import javax.jws.WebParam;
import java.util.List;

@Controller
public class CategoryController {

// KOD 13
    @Autowired
    private  BazaCategoryRepository bazaCategoryRepository;


    private CategoryRepository categoryRepository = new CategoryRepository();
    private GifRepository gifRepository= new GifRepository();


    @GetMapping("/categories")
    public String displayAll(ModelMap modelMap){
        modelMap.addAttribute("category",new Category()); //KOD 12 i 13 Dodawania kategori bez bazy danychi z bazą
        modelMap.addAttribute("categories", categoryRepository.findAll()); //KOD 12 i wcześniejsze - wyświetlanie z CategoryRepository bez bazy
        modelMap.addAttribute("categories2",bazaCategoryRepository.findAll());
        return "categories";
    }


     //KOD 13 dodawanie kategorii z bazą danych
    @PostMapping("/categories")
    public String displayNewAll(@ModelAttribute Category category, ModelMap modelMap){
        bazaCategoryRepository.save(category);
        modelMap.addAttribute("categories2",bazaCategoryRepository.findAll());
        modelMap.addAttribute("categories", categoryRepository.findAll());
        return "newCategories";
    }


    //KOD 13 dodawanie kategorii z bazą danych
    @GetMapping("/newcategories")
    public String displayAllNew (ModelMap modelMap){
        modelMap.addAttribute("category",new Category());
        modelMap.addAttribute("categories", bazaCategoryRepository.findAll());// KOD 13
        return "categories";
    }


/**KOD 12 Dodawanie kategorii bez bazy danych

    @PostMapping("/categories")
    public String displayNewAll(@ModelAttribute Category category, ModelMap modelMap){

        categoryRepository.addNew(category);
        modelMap.addAttribute("categories", categoryRepository.findAll());
        return "categories";
    }

    // KOD 12 Dodawanie kategorii bez bazy danych*/


    //kod 7
    @GetMapping("/category/{id}")
        public String displayCategory(@PathVariable int id, ModelMap modelMap){
          modelMap.addAttribute("category",categoryRepository.findByCategory(id));
            modelMap.addAttribute("gifs",gifRepository.findByCategoryId(id));
            return "category";
        }
        // kod 9



    // Teraz mam gify z dwóch Reposytoriów, dlatego przestało działać.
    @GetMapping("/categories/search")
            public String searchCategory (@RequestParam String q,ModelMap modelMap){
             List<Category> categoryList = categoryRepository.findCategoriesByNameIgnoreCase(q);
             if (categoryList.isEmpty()){
                 modelMap.addAttribute("categories",categoryRepository.findAll());

             }
             else
                 modelMap.addAttribute("categories", categoryList);
        return "categories";
    }
    //


    }


