package pl.akademiakodu.kwejk.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.kwejk.model.Gif;
import pl.akademiakodu.kwejk.repository.GifRepository;

import java.util.List;

@Controller
public class GifController {

    private GifRepository gifRepository = new GifRepository();

// KOD 4

    @GetMapping("/gif/{name}")
public String displayGif (@PathVariable String name, ModelMap modelMap){

        modelMap.addAttribute("gif",gifRepository.findByName(name).orElse(new Gif ("android-explosion",true)));
        return "gif-details";
    }
//KOD 4


// KOD 3

@GetMapping("/favorites")

public String getFavorites (ModelMap modelmap){

    modelmap.addAttribute("gifs",gifRepository.findFavorites());
    return "favorites";
}

//KOD 3

    //KOD 2

    @GetMapping("/")
    public String hello(ModelMap modelMap){
        modelMap.addAttribute("gifs", gifRepository.findAll());
        return "home";
    }

    // KOD 2

    /** KOD 1
    @GetMapping("/")
    public String hello(ModelMap modelMap){
        modelMap.addAttribute("gif", new Gif("compiler-bot"));
        return "home";
    }
   KOD 1 */

    //kod 8
    @GetMapping("/gifs/search")
            public String searchGif (@RequestParam String q,ModelMap modelMap){
        List<Gif> gifsList = gifRepository.findAllByNameIgnoreCase(q);
    if (gifsList.isEmpty())
        modelMap.addAttribute("gifs", gifRepository.findAll());
    else
        modelMap.addAttribute("gifs",gifsList);
        return "home";
    }


    //


}
