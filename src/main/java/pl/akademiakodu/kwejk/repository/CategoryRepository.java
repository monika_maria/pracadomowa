package pl.akademiakodu.kwejk.repository;

import pl.akademiakodu.kwejk.model.Category;
import pl.akademiakodu.kwejk.model.Gif;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CategoryRepository {

    private static List<Category> ALL_CATEGORIES = new ArrayList<>();

    static {
        ALL_CATEGORIES.add(new Category(1L,"Programming"));
        ALL_CATEGORIES.add(new Category(2L,"Fun"));
        ALL_CATEGORIES.add(new Category(3L,"Bot"));
    }

    // KOD 12 Dodawanie kategorii bez bazy danych

    public List<Category> addNew(Category category){
        ALL_CATEGORIES.add(category);
        return ALL_CATEGORIES;
    }



    //kod 7

    public Category findByCategory(int categoryId){
        return ALL_CATEGORIES.stream().filter(category -> category.getId() ==categoryId ).collect(Collectors.toList()).get(0);
    }
    //

    // kod 9

    public List<Category> findCategoriesByNameIgnoreCase(String name){
        return ALL_CATEGORIES.stream().filter(category->category.getName().toLowerCase()
                .contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }
    //
    public List<Category> findAll(){
        return ALL_CATEGORIES;
    }
}