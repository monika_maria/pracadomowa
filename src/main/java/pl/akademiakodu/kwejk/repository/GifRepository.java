package pl.akademiakodu.kwejk.repository;

import pl.akademiakodu.kwejk.model.Gif;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GifRepository {


    private static List<Gif> ALL_GIFS = new ArrayList<>();

    static{

        ALL_GIFS.add(new Gif ("android-explosion",true,"ben",1));
        ALL_GIFS.add(new Gif ("ben-and-mike",true,"michal",1));
        ALL_GIFS.add(new Gif ("book-dominos",true,"ada",2));
        ALL_GIFS.add(new Gif ("compiler-bot",false,"piotr",2));
        ALL_GIFS.add(new Gif ("cowboy-coder",false,"ola",3));
        ALL_GIFS.add(new Gif ("infinite-andrew",false,"adam",3));

    }
// kod 7
    public List<Gif> findByCategoryId( int categoryId){
        return ALL_GIFS.stream().filter(p->p.getCategoryId() == categoryId).collect(Collectors.toList());
    }
//
    public List<Gif> findAll(){

        return ALL_GIFS;
    }

    public static List<Gif> findFavorites() {
        return ALL_GIFS.stream().filter(p -> p.isFavorite()).collect(Collectors.toList());
    }

    public Optional<Gif> findByName(String name){

        return ALL_GIFS.stream().filter(p-> p.getName().equals(name)).findFirst();
    }
    //kod 8
    public List<Gif> findAllByNameIgnoreCase (String name){

        return ALL_GIFS.stream().filter(p->p.getName().toLowerCase().contains(name.toLowerCase())).collect(Collectors.toList());
    }
    //
}
