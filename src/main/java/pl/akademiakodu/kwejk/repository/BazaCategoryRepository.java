package pl.akademiakodu.kwejk.repository;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.kwejk.model.Category;

public interface BazaCategoryRepository extends CrudRepository<Category,Long> {
}
