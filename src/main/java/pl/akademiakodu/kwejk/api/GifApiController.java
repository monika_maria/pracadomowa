package pl.akademiakodu.kwejk.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.kwejk.model.Gif;
import pl.akademiakodu.kwejk.repository.GifRepository;

import java.util.List;

@RestController
public class GifApiController {

    private GifRepository gifRepository = new GifRepository();

    @GetMapping("/api/gifs")
    public List<Gif> gifs(){
        return gifRepository.findAll();
    }
    @GetMapping("api/gifs/find")
    public List<Gif> findGifs(@RequestParam String name){
        return gifRepository.findAllByNameIgnoreCase(name);
    }

}
