package pl.akademiakodu.kwejk.model;

import sun.net.www.content.image.gif;

public class Gif {
    private String name;
    private boolean favorite;
    private String username;

    //kod 7
    private int categoryId;
    //


    public Gif(String name,boolean favorite) {
        this.name = name;
        this.favorite = favorite;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public Gif(String name, boolean favorite, int categoryId) {
        this.name = name;
        this.favorite = favorite;
        this.categoryId = categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }


//kod 10
    /*public Gif (String name,boolean favorite,int categoryId, String username){
        this(name,favorite,categoryId);
        setUsername(username);
    }*/

    public Gif(String name, boolean favorite, String username, int categoryId) {
        this.name = name;
        this.favorite = favorite;
        this.categoryId = categoryId;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //KOD 10

    public String getThymeleafFilePath(){
        return new StringBuilder("").append("/gifs/").append(getName()).append(".gif").toString();
    }


    /*** inna opcja
     *    public String getThymeleafFilePath(){
     return "/gifs/" + "getName()" + ".gif";
     }
     */
}